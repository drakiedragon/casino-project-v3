﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using CDControlador;
using CDModelo;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data;
using MySql.Data.MySqlClient;
using MaterialDesignThemes.Wpf;

namespace VistaWPF
{
    /// <summary>
    /// Lógica de interacción para EditarProducto.xaml
    /// </summary>
    public partial class BuscarProducto : UserControl
    {
        EditarProducto editar = null;
        public int codigoEditar;
        public int codigoEliminar;
        ModelProducto objProducto = new ModelProducto();
        MySqlDataReader obtenerProducto;
        public BuscarProducto()
        {
            InitializeComponent();

            cargar_producto();
        }


        public static string Mid(string param, int startIndex, int length)

        {

            string result = param.Substring(startIndex, length);

            return result;

        }



        void cargar_producto()
        {
            string hay_datos = "0";




            obtenerProducto = objProducto.ListarProductos();



            while (obtenerProducto.Read() == true)
            {
                try
                {
                    if (obtenerProducto.GetInt32(8) == 0)
                    {
                        hay_datos = "1";
                        System.Windows.Media.Effects.DropShadowEffect dropShadowEffect = new System.Windows.Media.Effects.DropShadowEffect();
                        dropShadowEffect.ShadowDepth = 1;
                        dropShadowEffect.BlurRadius = 5;
                        dropShadowEffect.Direction = 270;
                        dropShadowEffect.Color = Color.FromRgb(34, 34, 34);

                        StackPanel cardMaestra = new StackPanel();
                        cardMaestra.Width = 520;
                        int codigo = obtenerProducto.GetInt32(0);
                        StackPanel card = new StackPanel();
                        card.Effect = dropShadowEffect;

                        Border borderContenido = new Border();
                        borderContenido.Padding = new Thickness(10);
                        borderContenido.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffffff"));
                        card.Orientation = Orientation.Vertical;
                        card.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4a74ae"));
                        card.Margin = new Thickness(20, 10, 20, 10);
                        card.Height = (double)Height;
                        TextBlock codigoProducto = new TextBlock();

                        asd.Children.Add(cardMaestra);
                        cardMaestra.Children.Add(card);
                        card.Children.Add(codigoProducto);
                        card.Children.Add(borderContenido);

                        StackPanel contenedorDatos = new StackPanel();
                        contenedorDatos.Orientation = Orientation.Vertical;
                        contenedorDatos.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffffff"));

                        borderContenido.Child = contenedorDatos;

                        StackPanel contenedorNombre = new StackPanel();
                        contenedorNombre.Orientation = Orientation.Horizontal;
                        contenedorDatos.Children.Add(contenedorNombre);

                        TextBlock nombreText = new TextBlock();
                        nombreText.FontFamily = new FontFamily("Roboto");
                        nombreText.TextWrapping = TextWrapping.Wrap;
                        nombreText.FontSize = 14;
                        nombreText.TextAlignment = TextAlignment.Center;
                        nombreText.FontWeight = FontWeights.Bold;
                        nombreText.Text = "Nombre:";
                        nombreText.Width = 90;
                        contenedorNombre.Children.Add(nombreText);

                        TextBlock nombreProducto = new TextBlock();
                        nombreProducto.FontFamily = new FontFamily("Roboto");
                        nombreProducto.TextWrapping = TextWrapping.Wrap;
                        nombreProducto.FontSize = 14;
                        nombreProducto.TextAlignment = TextAlignment.Left;
                        nombreProducto.FontWeight = FontWeights.Light;
                        nombreProducto.Text = obtenerProducto.GetString(1);
                        nombreProducto.Width = 360;
                        contenedorNombre.Children.Add(nombreProducto);

                        StackPanel contenedorMarca = new StackPanel();
                        contenedorMarca.Orientation = Orientation.Horizontal;
                        contenedorDatos.Children.Add(contenedorMarca);

                        TextBlock marcaText = new TextBlock();
                        marcaText.FontFamily = new FontFamily("Roboto");
                        marcaText.TextWrapping = TextWrapping.Wrap;
                        marcaText.FontSize = 14;
                        marcaText.TextAlignment = TextAlignment.Center;
                        marcaText.FontWeight = FontWeights.Bold;
                        marcaText.Text = "Marca:";
                        marcaText.Width = 90;
                        contenedorMarca.Children.Add(marcaText);

                        TextBlock marcaProducto = new TextBlock();
                        marcaProducto.FontFamily = new FontFamily("Roboto");
                        marcaProducto.TextWrapping = TextWrapping.Wrap;
                        marcaProducto.FontSize = 14;
                        marcaProducto.TextAlignment = TextAlignment.Left;
                        marcaProducto.FontWeight = FontWeights.Light;
                        marcaProducto.Text = obtenerProducto.GetString(7);
                        marcaProducto.Width = 360;
                        contenedorMarca.Children.Add(marcaProducto);

                        codigoProducto.Text = "Código:" + codigo;
                        codigoProducto.Padding = new Thickness(0, 5, 0, 5);
                        codigoProducto.FontSize = 16;
                        codigoProducto.TextAlignment = TextAlignment.Center;
                        codigoProducto.FontWeight = FontWeights.SemiBold;
                        codigoProducto.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffffff"));
                        codigoProducto.FontFamily = new FontFamily("Roboto");

                        Button buttonEditar = new Button();
                        buttonEditar.Tag = codigo;
                        buttonEditar.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4a74ae"));
                        buttonEditar.Content = "Editar producto";
                        buttonEditar.Click += new RoutedEventHandler(button_Click);
                        buttonEditar.Width = (cardMaestra.Width - 40) / 2;

                        Button buttonEliminar = new Button();
                        buttonEliminar.Tag = codigo;
                        buttonEliminar.Content = "Eliminar producto";
                        buttonEliminar.VerticalAlignment = VerticalAlignment.Center;
                        buttonEliminar.Click += new RoutedEventHandler(buttonE_Click);
                        buttonEliminar.Width = (cardMaestra.Width - 40) / 2;



                        buttonEliminar.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ce4040"));

                        StackPanel contenedorButtons = new StackPanel();
                        contenedorButtons.Orientation = Orientation.Horizontal;


                        card.Children.Add(contenedorButtons);
                        contenedorButtons.HorizontalAlignment = HorizontalAlignment.Center;
                        contenedorButtons.Children.Add(buttonEditar);
                        contenedorButtons.Children.Add(buttonEliminar);
                        contenedorButtons.VerticalAlignment = VerticalAlignment.Bottom;
                        card.Height = Height;

                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(messageBoxText: Convert.ToString(ex), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                }
               

            }
            if (hay_datos == "0")
            {
                System.Windows.Media.Effects.DropShadowEffect dropShadowEffect = new System.Windows.Media.Effects.DropShadowEffect();
                dropShadowEffect.ShadowDepth = 1;
                dropShadowEffect.BlurRadius = 5;
                dropShadowEffect.Direction = 270;
                dropShadowEffect.Color = Color.FromRgb(34, 34, 34);

                StackPanel cardMaestra = new StackPanel();
                cardMaestra.Width = 520;
                StackPanel card = new StackPanel();
                card.Effect = dropShadowEffect;


                TextBlock codigoProducto = new TextBlock();

                asd.Children.Add(cardMaestra);
                cardMaestra.Children.Add(card);
                card.Children.Add(codigoProducto);


                codigoProducto.Text = "No hay Datos.";
                codigoProducto.Padding = new Thickness(0, 5, 0, 5);
                codigoProducto.FontSize = 16;
                codigoProducto.TextAlignment = TextAlignment.Center;
                codigoProducto.FontWeight = FontWeights.SemiBold;
                codigoProducto.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffffff"));
                codigoProducto.FontFamily = new FontFamily("Roboto");

            }
        }

           

            void button_Click(object sender, EventArgs e)
            {
                editar = new EditarProducto();
                Button b = sender as Button;
                // identify which button was clicked and perform necessary actions

                Grid asd = (Grid)this.Parent;
                MessageBox.Show(asd.Children.Count.ToString());
                this.Visibility = Visibility.Hidden;
                asd.Children.Add(editar);
            MessageBox.Show(asd.Children.Count.ToString());
            int codigo = Convert.ToInt32(b.Tag.ToString());
                editar.DesignationName = codigo;
                editar.cargar_producto();
                editar.listarPadre = this;

            }

            void buttonE_Click(object sender, EventArgs e)
            {
                Button b = sender as Button;
                
                

                // identify which button was clicked and perform necessary actions
                ModelProducto objProducto = new ModelProducto();
                MySqlDataReader eliminar;
                objProducto.codigoProducto = Convert.ToInt32(b.Tag);

                if (MessageBox.Show("¿Seguro desea eliminar este elemento?", "Eliminar Producto", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    eliminar = objProducto.eliminarProducto();
                    eliminar.Read();
                    eliminar.Close();
                    MessageBox.Show(messageBoxText: "Cambios realizados, Producto eliminado.", caption: "Eliminar Producto", button: MessageBoxButton.OK, icon: MessageBoxImage.Information);
                    
                    
                    
                }
                else
                {
                    MessageBox.Show(messageBoxText: "Proceso cancelado.", caption: "Eliminar Producto", button: MessageBoxButton.OK, icon: MessageBoxImage.Information);
                }







            }

        }

    }
