﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data;


namespace CDControlador
{
    public class ConProducto
    {
        private ConDB conexion = new ConDB();
        private MySqlDataReader leer;

        public MySqlDataReader agregarProducto(int codigoProducto, string NombreProducto, int PrecioProducto, int StockProducto, int StockMinimo, int id_subcategoria, int EstadoProducto, int idMarca  )
        {
            string sql = "Insert into producto values(" + codigoProducto + ", '" + NombreProducto + "', " + PrecioProducto + "," + StockProducto + "," + StockMinimo + "," + id_subcategoria + "," + EstadoProducto + "," + idMarca + ")";

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader PedirNombreSubYCaProductoAeditar(int codigoProducto)
        {
            string sql = "select marca.nombre_marca, categoria.id_categoria, categoria.nombre_categoria, subcategoria.nombre_subcategoria from producto "+
                         "Inner join marca on producto.id_marca = marca.id_marca " +
                         "inner join subcategoria on producto.id_subcategoria = subcategoria.id_subcategoria " +
                         "inner join categoria on subcategoria.id_categoria = categoria.id_categoria " +
                         "where producto.codigo_producto = "+ codigoProducto;

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader ListarProducto()
        {
            //Solicita todos los datos del producto, se usara para listar todos los objetos producto
            string sql = "SELECT producto.codigo_producto, producto.nombre_producto, producto.precio_producto, producto.stock_producto, producto.stock_minimo, subcategoria.nombre_subcategoria, unidad_medida.nombre_unidad_medida, marca.nombre_marca, producto.estado_producto FROM producto " +
                            "INNER JOIN subcategoria ON producto.id_subcategoria = subcategoria.id_subcategoria " +
                            "INNER JOIN unidad_medida ON subcategoria.id_unidad_medida = unidad_medida.id_unidad_medida " +
                            "INNER JOIN marca ON producto.id_marca = marca.id_marca";
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader MostrarProductoPorCodigo(int codigo)
        {
            //solicita todos los datos del producto, se mostrara en el boton eliminar
            string sql = "SELECT producto.codigo_producto, producto.nombre_producto, producto.precio_producto, subcategoria.nombre_subcategoria, unidad_medida.nombre_unidad_medida, marca.nombre_marca FROM producto " +
                            "INNER JOIN subcategoria ON producto.id_subcategoria = subcategoria.id_subcategoria " +
                            "INNER JOIN unidad_medida ON subcategoria.id_unidad_medida = unidad_medida.id_unidad_medida " +
                            "INNER JOIN marca ON producto.id_marca = marca.id_marca " +
                            "WHERE producto.codigo_producto = " + codigo;
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader SolicitarProductoAEditar(int codigo)
        {
            //Este se usa para solicitar el producto a editar, solicita menos datos que el mostrarProductoPorCodigo
            string sql = "SELECT producto.nombre_producto, producto.precio_producto, subcategoria.nombre_subcategoria, marca.nombre_marca, categoria.nombre_categoria, producto.stock_minimo FROM producto " +
                            "INNER JOIN subcategoria ON producto.id_subcategoria = subcategoria.id_subcategoria " +
                            "INNER JOIN unidad_medida ON subcategoria.id_unidad_medida = unidad_medida.id_unidad_medida " +
                            "INNER JOIN categoria ON subcategoria.id_categoria = categoria.id_categoria " +
                            "INNER JOIN marca ON producto.id_marca = marca.id_marca " +
                            "WHERE producto.codigo_producto =" + codigo;
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader SolicitarUnidadesDeMedidaProducto()
        {
            //Este se usa para solicitar las unidades de medidas, para listarlas en un combobox
            string sql = "SELECT unidad_medida.id_unidad_medida, unidad_medida.nombre_unidad_medida FROM unidad_medida ";

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader SolicitarCategoriasDeProducto()
        {
            //Este se usa para solicitar las categorias de los productos, para listarlas en un combobox
            string sql = "SELECT categoria.nombre_categoria FROM categoria ";

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader SolicitarSubCategoriasDeProducto(int codigo)
        {
            //Este se usa para solicitar las Subcategorias de los productos, para listarlas en un combobox
            string sql = "SELECT subcategoria.nombre_subcategoria from subcategoria " +
                        " WHERE subcategoria.id_categoria = " + codigo;
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader SolicitarMarcasP()
        {
            //Este se usa para solicitar las Subcategorias de los productos, para listarlas en un combobox
            string sql = "SELECT marca.nombre_marca FROM marca ";

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader ActualizarProducto(string nombreProducto, int precioProducto, int subCategoria, int Marca, int codigoProducto, int stock_minimo)
        {
            string sql = "UPDATE producto " +
                "SET " +
                " producto.nombre_producto = ' " + nombreProducto + "' ," +
                " producto.precio_producto = " + precioProducto + "," +
                " producto.id_subcategoria = " + subCategoria + "," +
                " producto.id_marca = " + Marca + "," +
                " producto.stock_minimo = " + stock_minimo +
                " WHERE producto.codigo_producto = " + codigoProducto + ";";

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;

        }

        public MySqlDataReader SolicitarEstadoProducto(int codigo)
        {
            string sql = "SELECT producto.estado_producto FROM producto " +
                         "WHERE producto.codigo_producto = " + codigo;
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }
        public MySqlDataReader EliminarProductoPorCodigo(int codigo)
        {
            string sql = "UPDATE producto " +
                         "SET producto.estado_producto = 1 " +
                         "WHERE producto.codigo_producto = " + codigo;
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader SolicitarIDCategoriaProductoPorNombre(string nombre)
        {
            //Este se usa para solicitar las categorias de los productos, para listarlas en un combobox
            string sql = "SELECT categoria.id_categoria FROM categoria " +
                         "WHERE categoria.nombre_categoria = '" + nombre + "'";

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader SolicitarIDMarcaProductoPorNombre(string nombre)
        {
            //Este se usa para solicitar las categorias de los productos, para listarlas en un combobox
            string sql = "SELECT marca.id_marca FROM marca " +
                         "WHERE marca.nombre_marca = '" + nombre + "'";

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }

        public MySqlDataReader SolicitarIDSubCategoriaProductoPorNombre(string nombre)
        {
            //Este se usa para solicitar las categorias de los productos, para listarlas en un combobox
            string sql = "SELECT subcategoria.id_subcategoria FROM subcategoria " +
                         "WHERE subcategoria.nombre_subcategoria = '" + nombre + "'";

            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }
        /*public MySqlDataReader Mostrar_datos_usuario(String user, String email, String telefono)
        {
            String sql = "SELECT * FROM USUARIO WHERE USER = '" + user + "'";
            MySqlCommand comando = new MySqlCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            leer = comando.ExecuteReader();
            return leer;
        }
        */
    }
}
