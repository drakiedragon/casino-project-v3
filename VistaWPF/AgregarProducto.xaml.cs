﻿using CDModelo;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VistaWPF
{
    /// <summary>
    /// Lógica de interacción para AgregarProducto.xaml
    /// </summary>
    public partial class AgregarProducto : UserControl
    {
        public AgregarProducto()
        {
            InitializeComponent();
            cargar_Marcas_y_Categorias();
        }

        public void cargar_Marcas_y_Categorias()
        {
            String nombreSubcategoria = "";
            String nombreMarca = "";


            ModelProducto objProducto = new ModelProducto();

            MySqlDataReader categoriaP;
            MySqlDataReader marcaP;

            ComboMarcaProducto.Items.Clear();
            ComboSubcategoria.Items.Clear();

            categoriaP = objProducto.solicitarCategoriasP();
            while (categoriaP.Read() == true)
            {
                try
                {
                    ComboCategoria.Items.Add(categoriaP.GetString(0));

                }
                catch (Exception ex)
                {
                    MessageBox.Show(messageBoxText: Convert.ToString(ex), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                }
            }

            ComboSubcategoria.SelectedItem = nombreSubcategoria;
            categoriaP.Close();
            marcaP = objProducto.solicitarMarcasP();

            while (marcaP.Read() == true)
            {
                try
                {

                    ComboMarcaProducto.Items.Add(marcaP.GetString(0));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(messageBoxText: Convert.ToString(ex), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                }

            }
            ComboMarcaProducto.SelectedItem = nombreMarca;
        }

        private void comboCategoriaProducto_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ComboSubcategoria.Items.Clear();
            ModelProducto objProducto = new ModelProducto();
            string codigo_categoria;
            MySqlDataReader CategoriaP;
            MySqlDataReader subCategoriaP;

            if (ComboCategoria.SelectedItem.ToString() != "" && ComboCategoria.SelectedItem.ToString() != null)
            {
                CategoriaP = objProducto.solicitudCodigoCategoria(ComboCategoria.SelectedItem.ToString());
                CategoriaP.Read();
                codigo_categoria = CategoriaP.GetString(0);
                CategoriaP.Close();
                subCategoriaP = objProducto.solicitarSubCategoriasP(Convert.ToInt32(codigo_categoria));
                while (subCategoriaP.Read() == true)
                {
                    try
                    {
                        ComboSubcategoria.Items.Add(subCategoriaP.GetString(0));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(messageBoxText: Convert.ToString(ex), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);
                    }
                }
                ComboSubcategoria.IsEnabled = true;

            }
            else
            {
                MessageBox.Show(messageBoxText: "Ah dejado el Campo Vacio, esta accion no es recomendable, por favor seleccion un item de la lista.", caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);

            }

        }

        private void Btn_Ingresar_Producto(object sender, RoutedEventArgs e)
        {
            try
            {
                if (InputCodigoProducto.Text != "" && InputCodigoProducto.Text != "" && InputPrecioProducto.Text != "" && ComboSubcategoria.Text != "" && ComboSubcategoria.Text != "Tipo De Producto" && ComboSubcategoria.SelectedItem != null && ComboCategoria.Text != "" && ComboCategoria.Text != "Categoria" && ComboCategoria.SelectedItem != null && ComboMarcaProducto.Text != "" && ComboMarcaProducto.Text != "Marca" && ComboMarcaProducto.SelectedItem != null)
                {
                    string CodigoSubcategoria;
                    string CodigoMarca;
                    ModelProducto objProducto = new ModelProducto();
                    MySqlDataReader ingresarProducto;
                    MySqlDataReader solicitarProductoParaConfirmar;
                    MySqlDataReader codigoSubcategoriaP;
                    MySqlDataReader codigoMarcaP;

                    //Se solicita el codigo de subcategoria
                    codigoSubcategoriaP = objProducto.solicitudCodigoSubcategoria(ComboSubcategoria.SelectedItem.ToString());
                    codigoSubcategoriaP.Read();
                    CodigoSubcategoria = codigoSubcategoriaP.GetString(0);
                    codigoSubcategoriaP.Close();

                    objProducto.codigoProducto = Convert.ToInt32(InputCodigoProducto.Text);
                    objProducto.nombreProducto = InputNombreProducto.Text;
                    objProducto.precioProducto = Convert.ToInt32(InputPrecioProducto.Text);
                    objProducto.idSubCategoria = Convert.ToInt32(CodigoSubcategoria);

                    //Se solicita el codigo de la marca

                    codigoMarcaP = objProducto.solicitudCodigoMarca(ComboMarcaProducto.SelectedItem.ToString());
                    codigoMarcaP.Read();
                    CodigoMarca = codigoMarcaP.GetString(0);
                    codigoMarcaP.Close();

                    objProducto.idMarca = Convert.ToInt32(CodigoMarca);
                    objProducto.stockMinimo = Convert.ToInt32(InputStockMinimoProducto.Text);
                    ingresarProducto = objProducto.IgresarProductos();
                    ingresarProducto.Read();
                    ingresarProducto.Close();
                    solicitarProductoParaConfirmar = objProducto.solicitarProductoAeditar();
                    solicitarProductoParaConfirmar.Read();
                    MessageBox.Show(messageBoxText: " Ingreso Exitoso!, " +
                        " Datos Ingresados: " +
                        " ** Nombre Producto: " + solicitarProductoParaConfirmar.GetString(0) +
                        " ** Precio Producto:  $ " + solicitarProductoParaConfirmar.GetString(1) +
                        " ** Tipo De Producto: " + solicitarProductoParaConfirmar.GetString(2) +
                        " ** Marca de Producto: " + solicitarProductoParaConfirmar.GetString(3) +
                        " ** stock Minimo de Producto: " + solicitarProductoParaConfirmar.GetString(5), caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Information);
                    solicitarProductoParaConfirmar.Close();

                }
                else
                {
                    MessageBox.Show(messageBoxText: "Hay Campos Vacios, Rellene o Seleccione los Campos Restantes e intente nuevamente.", caption: "", button: MessageBoxButton.OK, icon: MessageBoxImage.Exclamation);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(messageBoxText: "Error!: " + ex, caption: "Error de ingreso", button: MessageBoxButton.OK, icon: MessageBoxImage.Error);

            }

        }
    }
}
