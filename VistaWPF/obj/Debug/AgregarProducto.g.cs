﻿#pragma checksum "..\..\AgregarProducto.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C7B1C14C00B7A57DE385E253FEF56C8BC820652E"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using VistaWPF;


namespace VistaWPF {
    
    
    /// <summary>
    /// AgregarProducto
    /// </summary>
    public partial class AgregarProducto : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 42 "..\..\AgregarProducto.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.Transitions.TransitioningContent TrainsitionigContentSlide;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\AgregarProducto.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputCodigoProducto;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\AgregarProducto.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputNombreProducto;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\AgregarProducto.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboMarcaProducto;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\AgregarProducto.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboCategoria;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\AgregarProducto.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox ComboSubcategoria;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\AgregarProducto.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputPrecioProducto;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\AgregarProducto.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputStockMinimoProducto;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SistemaGestionCasino;component/agregarproducto.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AgregarProducto.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TrainsitionigContentSlide = ((MaterialDesignThemes.Wpf.Transitions.TransitioningContent)(target));
            return;
            case 2:
            this.InputCodigoProducto = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.InputNombreProducto = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.ComboMarcaProducto = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.ComboCategoria = ((System.Windows.Controls.ComboBox)(target));
            
            #line 56 "..\..\AgregarProducto.xaml"
            this.ComboCategoria.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboCategoriaProducto_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.ComboSubcategoria = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.InputPrecioProducto = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.InputStockMinimoProducto = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            
            #line 74 "..\..\AgregarProducto.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Btn_Ingresar_Producto);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

