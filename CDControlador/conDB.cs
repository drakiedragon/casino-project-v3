﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace CDControlador
{
    public class ConDB
    {
        private MySqlConnection Connecting = new MySqlConnection("server=localhost;UID=dev;password=dev;database=casino;Persist Security Info=True;SslMode = none");

        public MySqlConnection AbrirConexion()
        {
            if (Connecting.State == System.Data.ConnectionState.Closed)

                Connecting.Open();
            return Connecting;

        }

        public MySqlConnection CerrarConexion()
        {
            if (Connecting.State == System.Data.ConnectionState.Open)

                Connecting.Close();
            return Connecting;
        }
    }
}
